# Vault Config example

Java application that accesses and manages sensitive information (such as passwords, private keys, confidential information, etc.) stored in Vault, a Hashicorp secrets management tool.

This project use the dependency https://github.com/BetterCloud/vault-java-driver to communicate with the Vault, authenticating itself and requesting information stored securely, without that information needing to be exposed in the application or in the source code.

To do that is necessary:

- Declare environments variables: 
  - VAULT_TOKEN: Token used to read the values in Vault (ex: export VAULT_TOKEN=your-token)
  - VAULT_URL: Url of your Hashicorp Vault (ex: export VAULT_URL=http://127.0.0.1:8200)
- Your vault need to have the secret engine kv (key value) v2 with:
  - secrets/hello path 
    - value key (add the value you want)
  - secrets/app_java path
    - username key (add the value you want)
    - password key (add the value you want)

## To run the project:
Execute:
- mvn clean package
- java -jar target/hashicorp_vault_get_values-1.0-SNAPSHOT.jar

