package org.example.vault;

import com.bettercloud.vault.Vault;
import com.bettercloud.vault.VaultConfig;
import com.bettercloud.vault.VaultException;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Hello world!
 *
 */
public class VaultConfigExample
{
    public static void main(String[] args) {

        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("src/main/resources/config.properties");
            prop.load(input);

            // Getting the Vault URL and Token from OS environment variables
            String vault_url = System.getenv("VAULT_URL");
            String vault_token = System.getenv("VAULT_TOKEN");

            String vaultHelloPath = prop.getProperty("vault.kv.secret.hello.path");
            String vaultAppJavaPath = prop.getProperty("vault.kv.secret.app_java.path");

            // Configuration related vault conection
            final VaultConfig vaultConfig = new VaultConfig()
                    .address(vault_url)
                    .token(vault_token)
                    .engineVersion(2)
                    .build();

            // Configuration related vault conection using namespaces
            //final VaultConfig vaultConfig = new VaultConfig()
            //        .address(vault_url)
            //        .token(vault_token)
            //        .nameSpace("MyNamespace")
            //        .build();

            // Autentica a aplicação com o Vault
            final Vault vault = new Vault(vaultConfig);

            // Read secrets from Vault Server (see vault secret path in config.properties)
            String username = vault.logical().read(vaultAppJavaPath).getData().get("username");
            String password = vault.logical().read(vaultAppJavaPath).getData().get("password");
            String secret = vault.logical().read(vaultHelloPath).getData().get("value");

            System.out.println("Secret value from kv/secret/hello: " + secret);
            System.out.println("Secret value from kv/secret/app_java: " + username);
            System.out.println("Secret value from kv/secret/app_java: " + password);

        } catch (VaultException e) {
            System.err.println("Erro ao ler segredo do Vault: " + e.getMessage());
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
